# Changelog
## [2.0.0] -2020-11-23
### Added
- Added login/logout route


## [1.4.0] - 2020-08-13
### Added
- Added search bar breed cat


## [1.3.0] - 2020-10-08
### Added
- Added page cats

## [1.2.0] - 2020-03-23
### Added
- Added page login


## [1.1.0] - 2020-03-23
### Added
- Feature update member
