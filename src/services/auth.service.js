import axios from "axios";
import jwt_decode from "jwt-decode";
const API_URL = "http://dashboardapi.gregorynorvene.com/";

const tokenIsExpired = (decoded)=> {
  const now = Date.now().valueOf() / 1000
  if (typeof decoded.exp !== 'undefined' && decoded.exp <= now) {
      return  false;
  }
  if (typeof decoded.exp !== 'undefined' && decoded.exp > now) {
      return true
  }
}


const checkAuth =  () => {
let  result = false ;
  const token = localStorage.getItem("user");
  if (!token) {

    return result;
  }else{
    const tokenDecode= jwt_decode(token);
    tokenIsExpired(tokenDecode)
    result = tokenIsExpired(tokenDecode)

  }
  return result
}

const  login = async (email, password) => {
  return axios
    .post(API_URL + "login", {
       email,
       password
    })
    .then((response) => {
      if (response.data.accessToken) {

        localStorage.setItem("user", JSON.stringify(response.data.accessToken.token));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");

};


export default {
  login,
  logout,
  checkAuth
};
