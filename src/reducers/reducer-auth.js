import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
} from "../actions/action-types";

const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
  ? { isLoggedIn: true }
  : { isLoggedIn: false };

export default function (state = initialState, action) {
  const { type } = action;

  switch (type) {

    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,

      };
    case LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,

      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,

      };
    default:
      return state;
  }
}
