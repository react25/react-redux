import { combineReducers } from 'redux';
import reducerMember from "./reducer-members";
import reducerCat from "./reducer-cats";
import auth from "./reducer-auth";



const rootReducer = combineReducers({
    members: reducerMember,
    cats: reducerCat,
    auth
});

export default rootReducer;
