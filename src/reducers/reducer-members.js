import {At_MEMBERS} from "../actions/action-types";

export default function reducerMember (state =[], action){
    switch (action.type) {
        case At_MEMBERS.READ_ALL :
            return action.payload;
        case At_MEMBERS.CREATE :
            return [...state, action.payload]
        case At_MEMBERS.DELETE :
            return state.filter((post) => {
                if (post.id === action.payload) {
                    return false
                } else {
                    return true
                }
            })
        case At_MEMBERS.UPDATE :
            return [...state, action.payload]
        default:
            return state
    }
}