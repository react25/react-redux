import {At_CATS} from "../actions/action-types";

export default function reducerCat (state =[], action){
    switch (action.type) {
        case At_CATS.READ_ALL :
            return action.payload;
        default:
            return state
    }
    
}
