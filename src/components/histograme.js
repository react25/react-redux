import Chart from "react-google-charts";
import React from 'react'

const Histogramme = () =>{

    return(
        <div>
            <Chart
                width={'600px'}
                height={'400px'}
                chartType="Bar"
                loader={<div>Loading Chart</div>}
                data={[
                    ['Year', 'Sales', 'Expenses', 'Profit'],
                    ['2017', 1000, 400, 200],
                    ['2018', 1170, 460, 250],
                    ['2019', 660, 1120, 300],
                    ['2020', 1030, 540, 350],
                ]}
                options={{
                    // Material design options
                    chart: {
                        title: 'Company Greg',
                        subtitle: 'Sales, Expenses, and Profit: 2017-2020',
                    },
                }}
                // For tests
                rootProps={{ 'data-testid': '2' }}
            />
        </div>
    )

}

export default Histogramme

