import React, { Fragment } from "react";

import Button from '@material-ui/core/Button';
import { useForm } from 'react-hook-form'
import TextField from "@material-ui/core/TextField";
import {Paper, Grid} from '@material-ui/core';
import { connect } from "react-redux";
import {createMembers} from '../actions/index';

const defaultValues = {
    TextField: ""
};

const  FormMember = props =>{
    const { register, handleSubmit, errors,reset } = useForm();
    const { createMembers } = props;
    const onSubmit = (data, e) => {
        e.preventDefault();
        createMembers(data);
    };
    return (
        <Fragment>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
                <Paper style={{ padding: 16, backgroundColor:'#ebf5f0' }}>
                    <Grid container alignItems="flex-start" spacing={2}>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-first_name-input-required"
                                label="First name"
                                type="text"
                                name="first_name"
                                autoComplete="first_name"
                                margin="normal"
                                variant="outlined"
                                error={!!errors.first_name}
                                inputRef={register({
                                    required: true
                                })}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-last_name-input-required"
                                label="Last name"
                                type="text"
                                name="last_name"
                                autoComplete="email"
                                margin="normal"
                                variant="outlined"
                                error={!!errors.last_name}
                                inputRef={register({
                                    required: true
                                })}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-email-input-required"
                                label="Email address"
                                type="email"
                                name="email"
                                autoComplete="email"
                                margin="normal"
                                variant="outlined"
                                error={!!errors.email}
                                inputRef={register({
                                    required: true,
                                    pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                })}
                            />
                            <p>{errors.email && "Invalid email address"}</p>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-city-input-required"
                                label="City"
                                type="text"
                                name="city"
                                autoComplete="city"
                                margin="normal"
                                variant="outlined"
                                error={!!errors.city}
                                inputRef={register({
                                    required: true
                                })}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Button type="submit" size="large" variant="contained">
                                Submit
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button size="large" variant="outlined" color="secondary"  onClick={() => {reset(defaultValues);}}>
                                Reset Form
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            </form>

        </Fragment>
    );
};
const mapDispatchToProps = { createMembers };
export default connect(null, mapDispatchToProps)(FormMember);
