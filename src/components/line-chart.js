import Chart from "react-google-charts";
import React from 'react'

const LineChart = () =>{

    return(
        <div>
            <Chart
                width={'600px'}
                height={'400px'}
                chartType="LineChart"
                loader={<div>Loading Chart</div>}
                data={[
                    ['x', 'Visitors'],
                    [0, 10],
                    [1, 10],
                    [2, 23],
                    [3, 17],
                    [4, 18],
                    [5, 9],
                    [6, 11],
                    [7, 27],
                    [8, 33],
                    [9, 40],
                    [10, 32],
                    [11, 35],
                ]}
                options={{
                    hAxis: {
                        title: 'Website',
                    },
                    vAxis: {
                        title: 'Millions',
                    },
                }}
                rootProps={{ 'data-testid': '1' }}
            />
        </div>
    )

}

export default LineChart