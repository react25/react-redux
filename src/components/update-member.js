import React, {Fragment} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useForm} from "react-hook-form";
import {Grid, Paper} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {updateMember} from "../actions";
import { purple } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import {connect} from "react-redux";

function  DialogUpdateMember (props){
    const [open, setOpen] = React.useState(false);
    const { register, errors,handleSubmit } = useForm();
    const { updateMember } = props;
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const onSubmit = (data, e) => {
        e.preventDefault();
       updateMember(data,props.itemId.id);
    };

    return (
        <Fragment>

            <IconButton aria-label="update" onClick={handleClickOpen}>
                <CreateIcon  fontSize="default" style={{color: purple[500]}} />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Update member:{props.itemId.first_name} </DialogTitle>
                        <Fragment>
                            <form onSubmit={handleSubmit(onSubmit)}   noValidate>
                                <Paper style={{ padding: 16, backgroundColor:'#ebf5f0' }}>
                                    <Grid container alignItems="flex-start" spacing={2}>
                                        <Grid item xs={6}>
                                            <TextField
                                                id="outlined-first_name-input-required"
                                                label="First name"
                                                type="text"
                                                name="first_name"
                                                autoComplete="first_name"
                                                margin="normal"
                                                defaultValue={props.itemId.first_name}
                                                variant="outlined"
                                                error={!!errors.first_name}
                                                inputRef={register({
                                                    required: true
                                                })}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                id="outlined-last_name-input-required"
                                                label="Last name"
                                                type="text"
                                                name="last_name"
                                                defaultValue={props.itemId.last_name}
                                                autoComplete="email"
                                                margin="normal"
                                                variant="outlined"
                                                error={!!errors.last_name}
                                                inputRef={register({
                                                    required: true
                                                })}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                id="outlined-email-input-required"
                                                label="Email address"
                                                type="email"
                                                name="email"
                                                defaultValue={props.itemId.email}
                                                autoComplete="email"
                                                margin="normal"
                                                variant="outlined"
                                                error={!!errors.email}
                                                inputRef={register({
                                                    required: true,
                                                    pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                                })}
                                            />
                                            <p>{errors.email && "Invalid email address"}</p>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                id="outlined-city-input-required"
                                                label="City"
                                                type="text"
                                                name="city"
                                                defaultValue={props.itemId.city}
                                                autoComplete="city"
                                                margin="normal"
                                                variant="outlined"
                                                error={!!errors.city}
                                                inputRef={register({
                                                    required: true
                                                })}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Button type="submit" size="large" variant="contained">
                                                update
                                            </Button>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Button size="large" variant="outlined" color="secondary" onClick={handleClose} >
                                                Cancel
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </form>
                        </Fragment>
            </Dialog>
        </Fragment>

    );
}
const mapDispatchToProps = { updateMember };
export default connect(null, mapDispatchToProps)(DialogUpdateMember);
