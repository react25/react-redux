import React, {Component} from 'react';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {bindActionCreators} from "redux";
import {connect} from "react-redux"
import {deleteMember, readAllMembers} from "../actions/index";
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import { CSSTransitionGroup } from 'react-transition-group'
import {Link} from "react-router-dom";
import AlertDialog from "./update-member";

const styles =  theme => ({
    table: {
        maxWidth: '100%',
        background: 'linear-gradient(45deg, #A4BADA 30%, #CBD6B1 80%)',
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',

    },
    spaceTabCell:{
    width: '200px',
    },
});

class MemberItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formOpen: true
        };
    }

    handleClickOpen = () => {
        this.props.formOpen(true);
        AlertDialog(this.state.formOpen)
    };

    UNSAFE_componentWillMount() {
        this.props.readAllMembers()
    }
    renderMember() {
        const {members,classes} = this.props
        let arrayMember
        if (members) {
            arrayMember= members
            return arrayMember.map((member) =>{
                return(
                      <TableRow key={member.id} hover={true}>
                        <TableCell component="th" scope="row">
                            {member.first_name}
                        </TableCell>
                        <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                        <TableCell align="center">{member.last_name}</TableCell>
                        <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                        <TableCell align="center">{member.email}</TableCell>
                        <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                        <TableCell align="center">{member.city}</TableCell>
                        <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                        <TableCell align="center" className={classes.spaceTabCell}>
                            <IconButton aria-label="delete" onClick={() => this.deleteMemberCallBack(member)}>
                                <DeleteOutlinedIcon fontSize="default" color="secondary" />
                            </IconButton>
                            <AlertDialog  itemId ={member}/>

                        </TableCell>
                    </TableRow>)
            })

        }
    }
    deleteMemberCallBack(member){
        this.props.deleteMember(member.id);

    }
        render(){
            const { classes } = this.props;
            return (
                <div>
                    <Button variant="contained" component={Link}  to="/CreateMember" color="primary">
                        Add New Member
                    </Button>
                    <Grid item xs={12} style={{height: '20px'}}></Grid>
                    <TableContainer component={Paper} >
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow hover={true}>
                                    <TableCell>First Name</TableCell>
                                    <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                                    <TableCell align="center">Last Name</TableCell>
                                    <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                                    <TableCell align="center">Email</TableCell>
                                    <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                                    <TableCell align="center">City</TableCell>
                                    <TableCell className={classes.spaceTabCell} align="center"></TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                                <CSSTransitionGroup component="tbody"
                                                         transitionEnterTimeout = {500}
                                                         transitionLeaveTimeout ={300}
                                                         transitionName="fade">
                                    {this.renderMember()}
                                </CSSTransitionGroup>
                        </Table>
                    </TableContainer>
                </div>
            )
        }

}
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({readAllMembers,deleteMember},dispatch)
})
const mapStateToProps =(state) =>{
    return{
        members:state.members
    }
}
MemberItem = withStyles(styles)(MemberItem);
export default connect (mapStateToProps,mapDispatchToProps) (MemberItem);
