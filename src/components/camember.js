import Chart from "react-google-charts";
import React from 'react'

const CircleChart = () =>{

    return(
        <div>
            <Chart
                width={'500px'}
                height={'500px'}
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                data={[
                    ['TV', 'Hours per Day'],
                    ['Computer', 11],
                    ['Housse', 2],
                    ['Computer', 2],
                    ['Travel', 2],
                    ['Investissement', 7],
                ]}
                options={{
                    title: 'Annual Activities',
                    // Just add this option
                    is3D: true,
                }}
                rootProps={{ 'data-testid': '2' }}
            />
        </div>
    )

}

export default CircleChart