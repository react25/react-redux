import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
const useStyles = makeStyles({
  root: {
     maxWidth: 345,
  },
  media: {
     height: 140,
  },
});

export default function MediaCard({cat}) {
  const classes = useStyles();
  return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={cat.url}
            title={cat.id}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {cat.breeds[0].name}

            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {cat.breeds[0].description}

            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
      <Button size="small" color="primary" href={cat.breeds[0].wikipedia_url} target="blank">
        wikipedia
      </Button>

    </CardActions>
      </Card>
  )
}
