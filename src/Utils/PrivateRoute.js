
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthService from "../services/auth.service";

function PrivateRoute({ component: Component, ...rest }) {

  return (
    <Route
      {...rest}
      render={(props) =>   AuthService.checkAuth()   ? <Component {...props} /> :  <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
    />
  )
}

export default PrivateRoute;
