import React from 'react'
import MiniDrawer from "../containers/drawer";
import Grid from "@material-ui/core/Grid";
import FormProfil from "../components/form-profil";

const Profil = () =>{

    return(
        <Grid container spacing={32}>
            <MiniDrawer>
            <h1>Profil</h1>
            <FormProfil/>
            </MiniDrawer>

        </Grid>
    )
}

export default Profil