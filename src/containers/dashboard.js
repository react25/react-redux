import React from 'react'
import Histogramme from "../components/histograme";
import LineChart from "../components/line-chart";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CircleChart from "../components/camember";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));
const Dashboard = () =>{
    const classes = useStyles();
    return(
        <div>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <Paper className={classes.paper}> <Histogramme/></Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Paper className={classes.paper}><LineChart/></Paper>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <Paper className={classes.paper}><CircleChart/></Paper>
                </Grid>
            </Grid>
        </div>
    )

}

export default Dashboard