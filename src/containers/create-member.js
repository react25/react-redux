import React from 'react'
import MiniDrawer from "../containers/drawer";
import Grid from "@material-ui/core/Grid";
import FormMember from "../components/form-member";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const CreateMember = () =>{
    return(
        <Grid container spacing={0}>
            <MiniDrawer>
                <Typography component="div">
                    <Box textAlign="center" fontWeight="fontWeightBold" fontSize={32} m={2}>
                        Create new member
                    </Box>
                </Typography>
            <FormMember/>
            </MiniDrawer>

        </Grid>
    )
}
export default CreateMember