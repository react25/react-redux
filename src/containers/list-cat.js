import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MediaCard from '../components/card-cat';
import {connect} from "react-redux"
import {readAllCats,readBreedCat} from "../actions/index";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import MiniDrawer from "./drawer";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from "@material-ui/core/Button";
const styles  = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
  margin: theme.spacing(1),
},
});


class ListCat extends Component {

  constructor(props) {
   super(props);
   this.state = {value: 'asho'};

   this.handleChange = this.handleChange.bind(this);
   this.handleSubmit = this.handleSubmit.bind(this);
 }

  UNSAFE_componentWillMount() {
      this.props.readAllCatsDefault();

  }
    handleChange = (event,state) => {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.readAllCatByBreed(this.state.value)
  }

    render(){
      const {cats,classes} = this.props
      return (

      <div className={classes.root}>
      <Grid container spacing={0}>
          <MiniDrawer>
            <Grid container item xs={12}  justify="center" alignItems="center">
              <Typography component="div">
                  <Box textAlign="center" fontWeight="fontWeightBold" fontSize={32} m={2}>
                      List of cats
                  </Box>
              </Typography>
              </Grid>

              <Grid container item xs={12}  justify="center" alignItems="center">

                <FormControl className={classes.formControl}  margin='dense'>
                 <InputLabel id="demo-controlled-open-select-label">Breed</InputLabel>
                 <Select
                   labelId="demo-controlled-open-select-label"
                   id="demo-controlled-open-select"
                   value={this.state.value}
                   onChange={this.handleChange}
                   variant="outlined"
                 >
                   <MenuItem value={"asho"}>American Shorthair</MenuItem>
                   <MenuItem value={"aege"}>Aegean</MenuItem>
                   <MenuItem value={"abob"}>American Bobtail</MenuItem>
                    <MenuItem value={"abys"}>Abyssinian</MenuItem>
                 </Select>

               </FormControl>


               <Button
               onClick={this.handleSubmit}
               variant="outlined"
               color="primary"
               className={classes.button}
               >
                Search
               </Button>

            </Grid>



              <Grid container spacing={2}>
                  {cats.map(row => (
                    <Grid item xs={12} sm={4}>

                      <MediaCard cat={row}/>
                    </Grid>
                  ))}

              </Grid>
          </MiniDrawer>

      </Grid>

      </div>
      );
    }
}

const mapDispatchToProps = dispatch => ({
  readAllCatsDefault: () => dispatch(readAllCats()),
  readAllCatByBreed: id => dispatch(readBreedCat(id))
});

const mapStateToProps =(state) =>{
    return{
        cats:state.cats
    }
}

ListCat = withStyles(styles)(ListCat);
export default  connect (mapStateToProps,mapDispatchToProps) (ListCat)
