import React from 'react'
import MiniDrawer from "./drawer";
import Grid from "@material-ui/core/Grid";
import TabMember from "../components/liste-item";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const ListMember = () =>{

    return(
        <Grid container spacing={0}>
            <MiniDrawer>
                <Typography component="div">
                    <Box textAlign="center" fontWeight="fontWeightBold" fontSize={32} m={2}>
                        List of members
                    </Box>
                </Typography>

              <TabMember/>
            </MiniDrawer>

        </Grid>
    )

}

export default ListMember
