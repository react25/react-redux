import React from "react";
import Typography from '@material-ui/core/Typography';
import { makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Link from '@material-ui/core/Link';
import {Grid} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Container from '@material-ui/core/Container';
import {useForm} from "react-hook-form";
import AuthService from "../services/auth.service";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    container:{
        display: 'flex',
        paddingTop:'20px',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',

    },
    cover: {
        flex: '1 0 auto',
        width: 600,
        backgroundColor: '#B0E0E6'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '50ch',
    },

}));


const  Register = props =>{
    const classes = useStyles();

    // const [loading, setLoading] = useState(false);
    const { register,handleSubmit } = useForm();


      const onSubmit = (data, e) => {
          e.preventDefault();
          AuthService.login(data.email, data.password).then(
         () => {
                // setLoading(true);
           props.history.push("/Members");
           window.location.reload();
         },
         (error) => {
           // setLoading(false);
         });
      }

     const handleLogin = () => {

     }

    return (
        <Container className={classes.container} >
            <Card className={classes.root}>
                <div className={classes.details}>
                    <CardContent className={classes.content}>
                        <form onSubmit={handleSubmit(onSubmit)} noValidate>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Typography variant="h3" >
                                        Sign In
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="outlined-email-input-required"
                                        fullWidth
                                        label="Email address"
                                        type="email"
                                        name="email"
                                        autoComplete="email"
                                        defaultValue="admin@51.la"
                                        margin="normal"
                                        variant="outlined"
                                        inputRef={register({
                                                  required: true
                                              })}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="outlined-email-input-required"
                                        fullWidth
                                        label="Password"
                                        type="text"
                                        name="password"
                                        autoComplete="password"
                                        defaultValue="password"
                                        margin="normal"
                                        variant="outlined"
                                        inputRef={register({
                                                  required: true
                                              })}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox icon={<FavoriteBorder />} checkedIcon={<Favorite />} name="checkedH" />}
                                    label="Remember Me"
                                />
                                </Grid>

                                <Grid item xs={12}>
                                <Button type="submit" size="large" variant="contained" color="primary"  fullWidth>
                                    Register
                                </Button>
                                </Grid>
                                <Grid item xs={6}>
                                    <Link href="#">
                                        Forget password?
                                    </Link>
                                </Grid>
                                <Grid item xs={6} >
                                    <Link href="#">
                                        Don't have an account? Sign Up
                                    </Link>
                                </Grid>
                                <Grid item xs={12} style={{alignItems:"center"}} onClick={handleLogin}>

                                    <Link href="#">
                                        Return to App
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </CardContent>
                </div>
                <CardMedia
                    className={classes.cover}
                    image="img/man.png"
                    title="Live from space album cover"
                />
            </Card>
        </Container>
    );
}


export default (Register);
