import React from 'react';
import Grid from '@material-ui/core/Grid';
import MiniDrawer from "./drawer";
import Dashboard from "./dashboard";
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function Layout() {

    return (
        <Grid container spacing={0}>
            <MiniDrawer>
                <Typography component="div">
                    <Box textAlign="center" fontWeight="fontWeightBold" fontSize={32} m={2}>
                       Dahsboard
                    </Box>
                </Typography>
                <Dashboard/>
            </MiniDrawer>

        </Grid>
    );
}

export default Layout;