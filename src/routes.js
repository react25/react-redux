import React,{Component}  from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFound from "./components/not-found";
import Layout from "./containers/layout";
import ListMember from "./containers/list-member";
import CreateMember from "./containers/create-member";
import Register from "./containers/login";
import ListCat from "./containers/list-cat";
import PublicRoute from './Utils/PublicRoute';
import PrivateRoute from './Utils/PrivateRoute';

class Routes extends Component{

    render(){

        return(
            <div>
                <Router >
                    <Switch>
                        <Route exact path="/" component={Layout}/>
                        <PrivateRoute path="/Members" component={ListMember}/>
                        <Route exact path="/Cats" component={ListCat}/>
                        <PrivateRoute path="/CreateMember" component={CreateMember}/>
                        <PublicRoute path="/Login" component={Register}/>
                        <Route component={NotFound}/>
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default Routes;
