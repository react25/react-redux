export const At_MEMBERS ={
    DELETE : "DELETE_MEMBER",
    CREATE : "CREATE_MEMBER",
    READ : "READ_MEMBER",
    READ_ALL : "READ_ALL_MEMBER",
    UPDATE: "UPDATE_MEMBER",
    ERROR : "ERROR"
}

export const  At_CATS ={
  READ_ALL : "READ_ALL_CAT",
  READ : "READ_CAT",

}


export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";
