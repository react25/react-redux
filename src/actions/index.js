import axios from 'axios'
import {At_MEMBERS,At_CATS} from "./action-types";

const END_POINT= "http://dashboardapi.gregorynorvene.com";

export function readAllMembers(){
    return function (dispatch){
        axios.get(`${END_POINT}/members`).then((response) =>{
            dispatch({type:At_MEMBERS.READ_ALL,payload:response.data})
        }).catch((error)=>{
            dispatch({type:At_MEMBERS.ERROR,payload:error.data})
        })
    }
}

export function deleteMember (id){
    return function (dispatch){
        axios.delete(`${END_POINT}/members/${id}`).then((response) =>{
            dispatch({type:At_MEMBERS.DELETE,payload:id})
        }).catch((error)=>{
            dispatch({type:At_MEMBERS.ERROR,payload:error.data})
        })
    }
}

export function updateMember(post, memberId){
    return function (dispatch){
        axios.put(`${END_POINT}/members/${memberId}`,{
            first_name: post.first_name,
            last_name: post.last_name,
            email: post.email ,
            city: post.city
        }).then((response) =>{
            dispatch({type:At_MEMBERS.CREATE,payload:response.data});
            window.location.href = 'members';
        }).catch((error)=>{
            dispatch({type:At_MEMBERS.ERROR,payload:error.data})
        })
    }
}


export  function createMembers(post){
    return function (dispatch){
        axios.post(`${END_POINT}/members`,{
            first_name: post.first_name,
            last_name: post.last_name,
            email: post.email ,
            city: post.city
        }).then((response) =>{
            dispatch({type:At_MEMBERS.CREATE,payload:response.data});
            window.location.href = '/';
        }).catch((error)=>{
            dispatch({type:At_MEMBERS.ERROR,payload:error.data})
        })
    }
}
export function readAllCats(){
    return function (dispatch){
        axios.get(`https://api.thecatapi.com/v1/images/search?limit=20&mime_types=&order=Random&size=small&breed_ids=asho`).then((response) =>{
            dispatch({type:At_CATS.READ_ALL,payload:response.data})
        }).catch((error)=>{
            dispatch({type:At_CATS.ERROR,payload:error.data})
        })
    }
}
  export function readBreedCat (id){
      console.log(id)
      return function (dispatch){

          axios.get(`https://api.thecatapi.com/v1/images/search?limit=20&mime_types=&order=Random&size=small&breed_ids=${id}`).then((response) =>{
              dispatch({type:At_CATS.READ_ALL,payload:response.data})
          }).catch((error)=>{
              dispatch({type:At_CATS.ERROR,payload:error.data})
          })
      }
  }
